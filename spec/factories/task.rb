# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    name { 'test' }
    description { 'prepare RSpec and more...' }
    # to use :user factory to generate asocciation named 'user' in Task model
    user
  end
end
