# frozen_string_literal: true

# use method 'factory' for define user class factory
FactoryBot.define do
  factory :user do
    name { 'test user' }
    email { 'test@sample.com' }
    password { 'password' }
  end
end
